﻿--
-- Date:           13-02-2017 15:28:23
-- Server version: 5.6.30-0ubuntu0.14.04.1
-- Host:           188.166.17.52
-- Database:       cip_test_db
-- User:           root
--

CREATE PROCEDURE `register_linkedin_user` (
            in i_first_name varchar(500),
            in i_last_name varchar(500),
            in i_email varchar(500),
            in i_password varchar(500),
            in i_is_expert varchar(50),
            in i_profile_summary text,
            in i_salt varchar(500),
            in i_linkedin_url varchar(500),
            out o_id int)
begin
DECLARE v_key INT DEFAULT 0;
DECLARE EXIT HANDLER FOR 1062 /* Duplicate key*/
  begin
    rollback;
  set o_id = -2;
END;
DECLARE exit handler for sqlexception
  BEGIN
  ROLLBACK;
  set o_id = -1;
END;
DECLARE exit handler for sqlwarning
 BEGIN
 ROLLBACK;
 set o_id = -1;
END;
START TRANSACTION;
	insert into user_master(email, is_expert, created_on) values(i_email, i_is_expert, now());
	set o_id = (select LAST_INSERT_ID());
	insert into user_details(user_id, first_name, last_name, profile_summary, linkedin_url) values(o_id, i_first_name, i_last_name, i_profile_summary, i_linkedin_url);
	set v_key = floor(rand() * 1000000);
	insert into user_credentials(user_id, password, is_verified, verification_key, salt) values(o_id, i_password, 'Y', v_key, i_salt);
	set o_id = v_key;
COMMIT;
END$$

CREATE PROCEDURE `register_linkedin_reviewer` (
            in i_first_name varchar(500),
            in i_last_name varchar(500),
            in i_email varchar(500),
            in i_password varchar(500),
            in i_profile_summary text,
            in i_salt varchar(500),
            in i_linkedin_url varchar(500),
            out o_id int)
begin
DECLARE v_key INT DEFAULT 0;
DECLARE EXIT HANDLER FOR 1062 /* Duplicate key*/
  begin
    rollback;
  set o_id = -2;
END;
DECLARE exit handler for sqlexception
  BEGIN
  ROLLBACK;
  set o_id = -1;
END;
DECLARE exit handler for sqlwarning
 BEGIN
 ROLLBACK;
 set o_id = -1;
END;
START TRANSACTION;
	insert into user_master(email, created_on) values(i_email, now());
	set o_id = (select LAST_INSERT_ID());
	insert into user_details(user_id, first_name, last_name, profile_summary, linkedin_url) values(o_id, i_first_name, i_last_name, i_profile_summary, i_linkedin_url);
	set v_key = floor(rand() * 1000000);
  insert into reviewer_master(reviewer_id, reviewer_status, is_other, datetime_responded) values (o_id, 'accepted', 'Y', now());
	insert into user_credentials(user_id, password, is_verified, verification_key, salt) values(o_id, i_password, 'Y', v_key, i_salt);
	set o_id = v_key;
COMMIT;
END$$

CREATE PROCEDURE `register_reviewer` (
            in i_first_name varchar(500),
            in i_last_name varchar(500),
            in i_email varchar(500),
            in i_password varchar(500),
            in i_salt varchar(500),
            out o_id int)
begin
DECLARE v_key INT DEFAULT 0;
DECLARE EXIT HANDLER FOR 1062 /* Duplicate key*/
  begin
    rollback;
  set o_id = -2;
END;
DECLARE exit handler for sqlexception
  BEGIN
  ROLLBACK;
  set o_id = -1;
END;
DECLARE exit handler for sqlwarning
 BEGIN
 ROLLBACK;
 set o_id = -1;
END;
START TRANSACTION;
	insert into user_master(email, created_on) values(i_email, now());
	set o_id = (select LAST_INSERT_ID());
	insert into user_details(user_id, first_name, last_name) values(o_id, i_first_name, i_last_name);
	set v_key = floor(rand() * 1000000);
  insert into reviewer_master(reviewer_id, reviewer_status, is_other, datetime_responded) values(o_id, 'accepted', 'Y', now());
	insert into user_credentials(user_id, password, verification_key, salt) values(o_id, i_password, v_key, i_salt);
	set o_id = v_key;
COMMIT;
END$$

CREATE PROCEDURE `save_application_comments` (
            in i_comment_id varchar(50),
            in i_application_id varchar(50),
            in i_user_id varchar(50),
            in i_comment_description varchar(5000),
            out o_id varchar(50))
begin
 if i_comment_id = '@' then
 insert into application_user_comments(application_id, user_id, comment_description) values(i_application_id, i_user_id, i_comment_description);
 set o_id = (select LAST_INSERT_ID());
 else
 update application_user_comments set comment_description = i_comment_description, status = 'edited' where
 comment_id = i_comment_id and
 application_id = i_application_id and
 user_id = i_user_id ;
 set o_id = i_comment_id;
end if;
END$$

CREATE PROCEDURE `register_user` (
            in i_first_name varchar(500),
            in i_last_name varchar(500),
            in i_email varchar(500),
            in i_password varchar(500),
            in i_is_expert varchar(50),
            in i_salt varchar(500),
            in i_reference varchar(500),
            out o_id int)
begin
DECLARE v_key INT DEFAULT 0;
DECLARE EXIT HANDLER FOR 1062 /* Duplicate key*/
  begin
    rollback;
  set o_id = -2;
END;
DECLARE exit handler for sqlexception
  BEGIN
  ROLLBACK;
  set o_id = -1;
END;
DECLARE exit handler for sqlwarning
 BEGIN
 ROLLBACK;
 set o_id = -1;
END;
START TRANSACTION;
	insert into user_master(email, is_expert, created_on) values(i_email, i_is_expert, now());
	set o_id = (select LAST_INSERT_ID());
	insert into user_details(user_id, first_name, last_name, reference) values(o_id, i_first_name, i_last_name, i_reference);
	set v_key = floor(rand() * 1000000);
	insert into user_credentials(user_id, password, verification_key, salt) values(o_id, i_password, v_key, i_salt);
	set o_id = v_key;
COMMIT;
END$$

CREATE PROCEDURE `get_cap_user_type_by_teamid` (IN i_user_id varchar(500), IN i_team_id varchar(500), OUT o_id int(11))
BEGIN
declare logged_in_user_type int default -1;
declare v_is_coach_exists int default 0;
declare v_is_manager int default 0;
declare v_is_member int default 0;
select count(*) into v_is_coach_exists from user_master where is_coach and user_id = i_user_id;
  if v_is_coach_exists > 0 then
    set logged_in_user_type =          1;   -- 1: coach
  else
    select count(*) into v_is_manager from cap_team_members where is_manager and user_id = i_user_id and team_id = i_team_id;
    if v_is_manager > 0 then
	     set logged_in_user_type = 2;   -- 2: manager
    else
       select count(*) into v_is_member from cap_team_members where user_id = i_user_id and team_id = i_team_id;
       if v_is_member > 0 then
          set logged_in_user_type = 3;   -- 3: member
       else
          set logged_in_user_type = 4;    -- 4: not a member
       end if;
    end if;
  end if;
  set o_id = logged_in_user_type;
end$$

CREATE PROCEDURE `get_cap_user_type` (IN i_user_id varchar(500), out o_id int(11))
BEGIN
declare logged_in_user_type int default -1;
declare v_is_coach_exists int default 0;
declare v_is_manager int default 0;
declare v_is_member int default 0;
select count(*) into v_is_coach_exists from user_master where is_coach and user_id = i_user_id;
  if v_is_coach_exists > 0 then
    set logged_in_user_type =          1;   -- 1: coach
  else
    select count(*) into v_is_manager from cap_team_members where is_manager and user_id = i_user_id;
    if v_is_manager > 0 then
	     set logged_in_user_type = 2;   -- 2: manager
    else
       select count(*) into v_is_member from cap_team_members where user_id = i_user_id;
       if v_is_member > 0 then
          set logged_in_user_type = 3;   -- 3: member
       else
          set logged_in_user_type = 4;    -- 4: not a member
       end if;
    end if;
  end if;
  set o_id = logged_in_user_type;
end$$

CREATE PROCEDURE `get_latest_post_details_in_thread` (
            in i_thread_id varchar(50),
            out o_user_id varchar(100),
            out o_user_name varchar(100),
            out o_created_on varchar(100))
begin
  declare main_id INT default 0;
  declare v_user_id varchar(100) default null;
  declare v_user_name varchar(100) default null;
  declare v_created_on varchar(100) default null;
  select pm.user_id, pm.created_on, concat_ws(' ', ud.first_name, ud.last_name) as user_name into v_user_id, v_created_on, v_user_name from forum_posts_master pm left join user_details ud using(user_id) where pm.thread_id = i_thread_id order by pm.created_on desc limit 1;
  if v_user_id is not null then
    set o_user_id = v_user_id;
  else
    set o_user_id = "";
  end if;
  if v_user_name is not null then
    set o_user_name = v_user_name;
  else
     set o_user_name = "";
  end if;
  if v_created_on is not null then
    set o_created_on = v_created_on;
  else
    set o_created_on = "";
  end if;
END$$

CREATE PROCEDURE `public_to_private` (INOUT i_room_id varchar(4000))
BEGIN
declare flag int default 0;
declare csr_user_id int default 0;
declare csr_mgr_id cursor for select distinct pm.user_id from forum_posts_master pm left join forum_threads_master tm using(thread_id) left join forum_rooms_master rm using(room_id) where pm.is_active and tm.is_closed = false and rm.room_id = i_room_id and pm.user_id not in(select user_id from forum_blocked_members);
declare CONTINUE HANDLER FOR NOT FOUND SET flag = 1;
open csr_mgr_id;
fdata : LOOP
fetch csr_mgr_id into csr_user_id;
IF flag = 1 THEN
LEAVE fdata;
END IF;
insert into forum_private_room_members(room_id, user_id) values(i_room_id,csr_user_id);
end LOOP fdata;
close csr_mgr_id;
END$$

CREATE PROCEDURE `get_user_type` (IN i_thread_id varchar(500), IN i_user_id varchar(500), out o_id int(11))
BEGIN
declare logged_in_user_type int default -1;
declare is_admin_exists int default 0;
declare is_moderator int default 0;
select count(*) > 0 into is_admin_exists from user_master where is_admin and user_id = i_user_id;
  if is_admin_exists > 0 then
    set logged_in_user_type = 1;
  else
    select count(*) > 0 into is_moderator from forum_moderators_master mm left join forum_rel_room_moderators rrm using(moderator_id) left join forum_threads_master tm using(room_id) where tm.thread_id = i_thread_id and mm.user_id = i_user_id;
    if is_moderator > 0 then
	     set logged_in_user_type = 2;
    else
	     set logged_in_user_type = 3;
    end if;
  end if;
  set o_id = logged_in_user_type;
end$$

CREATE PROCEDURE `save_application_review` (
            in i_evaluation_metric_1_score varchar(11),
            in i_evaluation_metric_1_text varchar(10000),
            in i_evaluation_metric_2_score varchar(11),
            in i_evaluation_metric_2_text varchar(10000),
            in i_evaluation_metric_3_score varchar(11),
            in i_evaluation_metric_3_text varchar(10000),
            in i_evaluation_metric_4_score varchar(11),
            in i_evaluation_metric_4_text varchar(10000),
            in i_evaluation_metric_5_score varchar(11),
            in i_evaluation_metric_5_text varchar(10000),
            in i_review_id varchar(50),
            in i_reviewer_id int(11),
            in i_review_status varchar(11),
            in i_application_id int(11),
            in i_review_comments_text varchar(1000),
            out o_id varchar(50))
begin
 if i_review_id = '@' then
  insert into review_master(application_id, reviewer_id, review_status, completed_on) values(i_application_id, i_reviewer_id, i_review_status, now());
  set o_id = (select LAST_INSERT_ID());
  insert into innovation_review_details(review_id, evaluation_metric_1_score, evaluation_metric_1_text, evaluation_metric_2_score,evaluation_metric_2_text,evaluation_metric_3_score, evaluation_metric_3_text, evaluation_metric_4_score,evaluation_metric_4_text,evaluation_metric_5_score,evaluation_metric_5_text,review_comments_text) values(o_id, i_evaluation_metric_1_score, i_evaluation_metric_1_text, i_evaluation_metric_2_score,i_evaluation_metric_2_text,i_evaluation_metric_3_score, i_evaluation_metric_3_text, i_evaluation_metric_4_score,i_evaluation_metric_4_text,i_evaluation_metric_5_score,i_evaluation_metric_5_text,i_review_comments_text);
 else
  update review_master set review_status = i_review_status, completed_on = now() where application_id = i_application_id and reviewer_id = i_reviewer_id and review_id = i_review_id;
  update innovation_review_details set evaluation_metric_1_score = i_evaluation_metric_1_score, evaluation_metric_1_text = i_evaluation_metric_1_text,evaluation_metric_2_score = i_evaluation_metric_2_score,evaluation_metric_2_text = i_evaluation_metric_2_text,evaluation_metric_3_score = i_evaluation_metric_3_score, evaluation_metric_3_text = i_evaluation_metric_3_text,evaluation_metric_4_score = i_evaluation_metric_4_score, evaluation_metric_4_text = i_evaluation_metric_4_text,evaluation_metric_5_score = i_evaluation_metric_5_score, evaluation_metric_5_text = i_evaluation_metric_5_text,review_comments_text= i_review_comments_text where review_id = i_review_id;
  set o_id = i_review_id;
 end if;
END$$

CREATE PROCEDURE `save_pp_learning_post` (
            in i_user_id varchar(100),
            in i_title varchar(500),
            in i_category_id varchar(100),
            in i_short_description varchar(5000),
            in i_long_description varchar(10000),
            in i_url_1 varchar(500),
            in i_url_2 varchar(500),
            in i_url_3 varchar(500),
            in i_upload_1 varchar(500),
            in i_upload_2 varchar(500),
            in i_upload_3 varchar(500),
            in i_upload_1_file_name varchar(500),
            in i_upload_2_file_name varchar(500),
            in i_upload_3_file_name varchar(500),
            in i_is_published bool,
            in i_action varchar(100),
            in i_pp_lp_id varchar(100),
            out o_id int(11))
begin
DECLARE u_id INT default 0;
DECLARE v_status varchar(100) default 'draft';
  select user_id into u_id from user_master where user_id = i_user_id;
  if u_id > 0 then		-- for a valid user
	 if i_is_published then		-- set the value of 'status' field
		set v_status = 'published';
	 else
		set v_status = 'draft';
	 end if;
	 if i_action like 'create' then
		insert into pp_lp_master(user_id, status, updated_on) values(u_id, v_status, now());
		set o_id = (select LAST_INSERT_ID());
	else 	-- for update
	  select pp_lp_id into o_id from pp_lp_master where pp_lp_id = i_pp_lp_id and user_id = i_user_id;
      update pp_lp_master set status = v_status, updated_on = now() where pp_lp_id = o_id;
      update pp_lp_details set is_current = false where pp_lp_id = o_id;
	end if;
  	insert into pp_lp_details(pp_lp_id, is_current, title, category_id, short_description,
				long_description, url_1, url_2, url_3, upload_1, upload_2, upload_3, upload_1_file_name, upload_2_file_name, upload_3_file_name)
        values(o_id, true, i_title, i_category_id, i_short_description, i_long_description,
				i_url_1, i_url_2, i_url_3, i_upload_1, i_upload_2, i_upload_3, i_upload_1_file_name, i_upload_2_file_name, i_upload_3_file_name);
  else
    set o_id = 0;
  end if;
END$$

CREATE PROCEDURE `save_marketplace` (
            in i_user_id varchar(100),
            in i_title varchar(500),
            in i_website varchar(100),
            in i_contact_email varchar(100),
            in i_phone_country_code varchar(100),
            in i_phone_number varchar(100),
            in i_summary varchar(10000),
            in i_image_name varchar(100),
            in i_is_published bool,
            in i_action varchar(100),
            in i_marketplace_id varchar(100),
            out o_id int(11))
begin
DECLARE u_id INT default 0;
DECLARE v_status varchar(100) default 'draft';
  select user_id into u_id from user_master where user_id = i_user_id;
  if u_id > 0 then		-- for a valid user
	 if i_is_published then		-- set the value of 'status' field
		set v_status = 'published';
	 else
		set v_status = 'draft';
	 end if;
	 if i_action like 'create' then
		insert into marketplace_master(user_id, status, updated_on) values(u_id, v_status, now());
		set o_id = (select LAST_INSERT_ID());
	else 	-- for update
	  select marketplace_id into o_id from marketplace_master where marketplace_id = i_marketplace_id and user_id = i_user_id;
      update marketplace_master set status = v_status, updated_on = now() where marketplace_id = o_id;
      update marketplace_details set is_current = false where marketplace_id = o_id;
	end if;
  	 insert into marketplace_details(marketplace_id, is_current, title, website, contact_email,
				phone_country_code_id, phone_number, summary, image_name)
        values(o_id, true, i_title, i_website, i_contact_email,
				i_phone_country_code, i_phone_number, i_summary, i_image_name);
  else
    set o_id = 0;
  end if;
END$$

CREATE PROCEDURE `save_template_details` (
            in i_temp_id varchar(50),
            in i_title varchar(1000),
            in i_description varchar(5000),
            out o_id int(50))
begin
declare v_count int default 0;
 if i_temp_id = '@' then
    select count(*) into v_count from template_details where title = i_title;
    if v_count > 0 then
        set o_id = -1; -- there is an active template which has the same title
    else
        insert into template_details(temp_id, title, description ,created_on ,updated_on) values(i_temp_id, i_title, i_description, CURTIME(), CURTIME());
        set o_id = (select LAST_INSERT_ID());
    end if;
 else
    select count(*) into v_count from template_details where title = i_title and temp_id <> i_temp_id;
   	if v_count > 0 then
        set o_id = -2; -- there is an active template which has the same title
    else
        update template_details set title = i_title, description = i_description , updated_on = CURTIME() where
        temp_id = i_temp_id ;
        set o_id = i_temp_id;
    end if;
end if;
END$$

CREATE PROCEDURE `sp_user_login_frequency` (IN inter INT, IN lim INT)
BEGIN
 select display_date, user.users,innovator.innovators,expert.experts from
 (select date_format(al.created_on, '%M, %Y') as display_date, count(*) as users, MONTH(al.created_on) as month, YEAR(al.created_on) as year
 from application_logs al left join user_master um on um.email = al.user
 where ((al.action='log_login' and al.text='successful') or al.text like '%logged in at%')
 and al.created_on BETWEEN DATE_SUB(CURDATE(), INTERVAL inter MONTH) AND CURDATE()
 GROUP BY MONTH(al.created_on)
 ) user
 left join
 (select date_format(al.created_on, '%M, %Y') as display_date, count(*) as innovators, MONTH(al.created_on) as month, YEAR(al.created_on) as year
 from application_logs al left join user_master um on um.email = al.user
 where ((al.action='log_login' and al.text='successful') or al.text like '%logged in at%') AND um.is_expert='N'
 and al.created_on BETWEEN DATE_SUB(CURDATE(), INTERVAL inter MONTH) AND CURDATE()
 GROUP BY MONTH(al.created_on)
 ) innovator
 using (display_date)
 left join
 (select date_format(al.created_on, '%M, %Y') as display_date, count(*) as experts, MONTH(al.created_on) as month, YEAR(al.created_on) as year
 from application_logs al left join user_master um on um.email = al.user
 where ((al.action='log_login' and al.text='successful') or al.text like '%logged in at%') AND um.is_expert='Y'
 and al.created_on BETWEEN DATE_SUB(CURDATE(), INTERVAL inter MONTH) AND CURDATE()
 GROUP BY MONTH(al.created_on)
 ) expert
using (display_date)
ORDER BY user.year desc, user.month desc LIMIT lim;
END$$

CREATE PROCEDURE `save_user_profile_details` (
            in i_user_id int(11),
            in i_email varchar(500),
            in i_salutation varchar(10),
            in i_profile_summary varchar(10000),
            in i_country varchar(100),
            in i_contact_email varchar(100),
            in i_country_code varchar(100),
            in i_phone_number varchar(100),
            in i_skype_id varchar(100),
            in i_website_url varchar(100),
            in i_linkedin_url varchar(100),
            in i_zipcode varchar(100),
            in i_image_name varchar(10),
            out o_id int)
begin
DECLARE u_id INT default 0;
/**
DECLARE exit handler for sqlexception
  BEGIN
  ROLLBACK;
  set o_id = -1;
END;
DECLARE exit handler for sqlwarning
 BEGIN
 ROLLBACK;
 set o_id = -1;
END;  */
  /** Authenticate user */
  select user_id into u_id from user_master where user_id = i_user_id and email = i_email;
  if u_id > 0 then
    UPDATE user_details
      SET
        salutation = i_salutation -- enum('Dr','Mr','Ms')
        ,contact_email = i_contact_email -- varchar(100)
        ,phone_country_code = i_country_code -- varchar(10)
        ,phone_number = i_phone_number -- varchar(100)
        ,skype_id = i_skype_id -- varchar(100)
        ,website_url = i_website_url -- varchar(100)
        ,linkedin_url = i_linkedin_url -- varchar(100)
        ,location_id = i_country -- varchar(100)
        ,profile_summary = i_profile_summary -- varchar(1000)
        ,zip_code = i_zipcode -- varchar(100)
        ,image_name = i_image_name -- enum('Y','N')
      WHERE
        user_id = u_id;
	 set o_id = 1;
  else
    set o_id = 0;
  end if;
END$$

CREATE PROCEDURE `save_discovery_opportunity_details` (
            in i_headline varchar(50),
            in i_status varchar(50),
            in i_deadline varchar(50),
            in i_image_name varchar(100),
            in i_description varchar(10000),
            in i_attachment_name_1 varchar(100),
            in i_attachment_name_2 varchar(100),
            in i_attachment_display_name_1 varchar(100),
            in i_attachment_display_name_2 varchar(100),
            in i_is_external bool,
            in i_external_link varchar(1000),
            in i_application_start_date varchar(50),
            in i_voting_deadline varchar(50),
            in i_no_of_winners varchar(10),
            in i_award_details varchar(1000),
            in i_opportunity_type varchar(50),
            in i_sponsor_name1 varchar(100),
            in i_sponsor_image1 varchar(500),
            in i_sponsor_name2 varchar(100),
            in i_sponsor_image2 varchar(500),
            in i_sponsor_name3 varchar(100),
            in i_sponsor_image3 varchar(500),
            in i_first_evaluation_criteria varchar(100),
            in i_second_evaluation_criteria varchar(100),
            in i_first_criteria_description varchar(500),
            in i_second_criteria_description varchar(500),
            in i_opportunity_id varchar(50),
            in i_voting_start_date varchar(50),
            out o_id int(11))
begin
 declare s_is_status varchar(20);
 declare v_count int default 0;
 if i_opportunity_id = '@' then
	select count(*) into v_count from opportunities where headline = i_headline and is_deleted = 'N';
	if v_count > 0 then
		set o_id = -1; -- there is an active opportunity which has the same headline
	else
		insert into opportunities(headline, deadline, status, image_name,description,attachment_name_1,
			attachment_name_2,is_external, external_link, attachment_display_name_1,attachment_display_name_2,
			application_start_date, voting_deadline, no_of_winners, award_amount, opportunity_type, sponsor_name1,
			sponsor_image1, sponsor_name2, sponsor_image2,sponsor_name3, sponsor_image3,
			first_evaluation_criteria,second_evaluation_criteria, first_criteria_description,second_criteria_description, voting_start_date)
		values(i_headline,i_deadline,i_status,i_image_name, i_description, i_attachment_name_1,
			i_attachment_name_2, i_is_external, i_external_link, i_attachment_display_name_1,
			i_attachment_display_name_2, i_application_start_date, i_voting_deadline, i_no_of_winners,
			i_award_details,  i_opportunity_type, i_sponsor_name1,i_sponsor_image1, i_sponsor_name2,
			i_sponsor_image2, i_sponsor_name3,i_sponsor_image3, i_first_evaluation_criteria,i_second_evaluation_criteria,
			i_first_criteria_description,i_second_criteria_description, i_voting_start_date);
		set o_id = (select LAST_INSERT_ID());
	end if;
  else
	select count(*) into v_count from opportunities where headline = i_headline and opportunity_id != i_opportunity_id and is_deleted = 'N';
	if v_count > 0 then
		set o_id = -2; -- there is another opportunity with same headline and is active
	else
		 update opportunities set
		 headline = i_headline,
		 status = i_status,
		 deadline = i_deadline,
		 image_name = i_image_name,
		 description = i_description,
		 attachment_name_1 = i_attachment_name_1,
		 attachment_name_2 = i_attachment_name_2,
		 attachment_display_name_1 = i_attachment_display_name_1,
		 attachment_display_name_2 = i_attachment_display_name_2,
		 is_external = i_is_external,
		 external_link = i_external_link,
		 application_start_date = i_application_start_date,
		 voting_deadline = i_voting_deadline,
		 no_of_winners = i_no_of_winners,
		 award_amount = i_award_details,
		 opportunity_type = i_opportunity_type,
		 sponsor_name1 = i_sponsor_name1,
		 sponsor_image1 = i_sponsor_image1,
		 sponsor_name2 = i_sponsor_name2,
		 sponsor_image2 = i_sponsor_image2,
		 sponsor_name3 = i_sponsor_name3,
		 sponsor_image3 = i_sponsor_image3,
		 first_evaluation_criteria = i_first_evaluation_criteria,
		 second_evaluation_criteria = i_second_evaluation_criteria,
		 first_criteria_description = i_first_criteria_description,
		 second_criteria_description = i_second_criteria_description,
		 voting_start_date = i_voting_start_date where opportunity_id = i_opportunity_id;
		 set o_id = i_opportunity_id;
	end if;
end if;
END$$

CREATE PROCEDURE `save_discovery_application` (
            in i_application_id varchar(50),
            in i_opportunity_id varchar(50),
            in i_user_id varchar(50),
            in i_application_name varchar(1000),
            in i_is_submitted varchar(50),
            in i_img_name varchar(500),
            out o_id varchar(50))
begin
declare v_final_submit varchar(50) default 0;
declare a_count int default 0;
if i_is_submitted = 'Y' then
    set v_final_submit = now();
end if;
if i_application_id = '@' then
	select count(*) into a_count from application_details ad left join application_master am on ad.application_id = am.application_id
		where application_name = i_application_name and is_active = 'Y';
	if a_count > 0 then
		set o_id = -1; -- tried to creaete new application but same name exist
	else
		insert into application_master(opportunity_id, user_id, is_submitted, final_submitted_on) values(i_opportunity_id, i_user_id,i_is_submitted,v_final_submit);
		set o_id = (select LAST_INSERT_ID());
		insert into application_details(application_id, application_name, img_name) values(o_id, i_application_name, i_img_name);
		insert into user_applications(user_id, application_id , is_principal) values(i_user_id,o_id , 'Y');
	end if;
else -- UPDATE
	select count(*) into a_count from application_details ad left join application_master am on ad.application_id = am.application_id
		where application_name = i_application_name and am.application_id != i_application_id and is_active = 'Y';
	if a_count > 0 then
		set o_id = -2; -- not allowed as there is another application which has that name
	else
		update application_details set application_name = i_application_name, img_name = i_img_name where application_id = i_application_id;
		update application_master set is_submitted = i_is_submitted, final_submitted_on = v_final_submit where application_id = i_application_id;
		set o_id = i_application_id;
	end if;
end if;
END$$

CREATE PROCEDURE `save_innovation_details` (
            in i_user_id int(11),
            in i_email varchar(500),
            in i_innovation_id int(11),
            in i_name varchar(500),
            in i_location_id varchar(50),
            in i_type_id varchar(50),
            in i_clinical_area_id varchar(50),
            in i_stage_id varchar(50),
            in i_org_type_id varchar(50),
            in i_source_id varchar(50),
            in i_description text,
            in i_facebook_url varchar(500),
            in i_twitter_url varchar(500),
            in i_google_plus_url varchar(500),
            in i_url_1 varchar(500),
            in i_url_2 varchar(500),
            in i_url_3 varchar(500),
            in i_fund_raised varchar(50),
            in i_submited_as varchar(50),
            out o_id int(11),
            in i_created_on varchar(50))
begin
DECLARE u_id INT default 0;
DECLARE v_version_code INT default 0;
/**
DECLARE exit handler for sqlexception
  BEGIN
  ROLLBACK;
  set o_id = -1;
END;
DECLARE exit handler for sqlwarning
 BEGIN
 ROLLBACK;
 set o_id = -1;
END; */
  select user_id into u_id from user_master where user_id = i_user_id and email = i_email;
  if u_id > 0 then
	 if i_innovation_id = 0 then
	   /** insert */
  	 insert into innovation_master(created_on, is_published) values(now(), i_submited_as);
  	 set o_id = (select LAST_INSERT_ID());
  	 insert into user_innovations(user_id, innovation_id, is_admin, joined_on) values(u_id, o_id, 'Y', i_created_on);
  	 insert into innovation_details(innovation_id,
  		name, location_id, innovation_type_id, clinical_area_id, innovation_stage_id, org_type_id, innovation_source_id, description,
  			facebook_url, twitter_url, google_plus_url, url_1, url_2, url_3, fund_raised)
        values(o_id, i_name, i_location_id, i_type_id, i_clinical_area_id, i_stage_id, i_org_type_id, i_source_id, i_description,
  				i_facebook_url, i_twitter_url, i_google_plus_url, i_url_1, i_url_2, i_url_3, i_fund_raised);
    else
      select innovation_id into o_id from user_master m,user_innovations i
       where m.user_id = i.user_id and m.email = i_email and i.user_id = u_id and innovation_id = i_innovation_id;
      update innovation_master set is_published = i_submited_as where innovation_id = i_innovation_id;
      select max(version_code) into v_version_code from innovation_details where innovation_id = i_innovation_id;
      set v_version_code = v_version_code + 1;
      update innovation_details set is_current = 'N' where innovation_id = o_id;
      insert into innovation_details(innovation_id, version_code,
  		name, location_id, innovation_type_id, clinical_area_id, innovation_stage_id, org_type_id, innovation_source_id, description,
  			facebook_url, twitter_url, google_plus_url, url_1, url_2, url_3, fund_raised)
        values(o_id, v_version_code ,i_name, i_location_id, i_type_id, i_clinical_area_id, i_stage_id, i_org_type_id, i_source_id, i_description,
  				i_facebook_url, i_twitter_url, i_google_plus_url, i_url_1, i_url_2, i_url_3, i_fund_raised);
	 end if;
  else
    set o_id = 0;
  end if;
END$$

CREATE PROCEDURE `save_job_post` (
            in i_is_linked_with_innovation bool,
            in i_post_for_id varchar(500),
            in i_post_for_name varchar(500),
            in i_title varchar(500),
            in i_user_id varchar(500),
            in i_contact_email varchar(500),
            in i_phone_number varchar(500),
            in i_phone_country_code varchar(500),
            in i_city_name varchar(500),
            in i_location_id varchar(500),
            in i_job_type varchar(500),
            in i_salary_type varchar(500),
            in i_salary_details varchar(1000),
            in i_short_description varchar(1000),
            in i_long_description varchar(10000),
            in i_qualification varchar(10000),
            in i_experience varchar(10000),
            in i_is_published bool,
            in i_action varchar(500),
            in i_job_post_id varchar(500),
            out o_id int(11))
begin
DECLARE u_id INT default 0;
DECLARE v_status varchar(100) default 'draft';
  select user_id into u_id from user_master where user_id = i_user_id;
  if u_id > 0 then
	 if i_is_published then
		set v_status = 'published';
	 else
		set v_status = 'draft';
	 end if;
	 if i_action like 'create' then
		insert into job_post_master(user_id, status, updated_on) values(u_id, v_status, now());
		set o_id = (select LAST_INSERT_ID());
	else 	-- for update
	  select job_post_id into o_id from job_post_master where job_post_id = i_job_post_id and user_id = i_user_id;
      update job_post_master set status = v_status, updated_on = now() where job_post_id = o_id;
      update job_post_details set is_current = 'N' where job_post_id = o_id;
	end if;
  	 insert into job_post_details(job_post_id, is_current, is_linked_with_innovation, inno_or_org_id, posted_for_name, title, contact_email,
				phone_country_code, phone_number, short_description, long_description,
				salary_type, city_name, location_id, job_type, salary_details, qualification, experience)
        values(o_id, 'Y', i_is_linked_with_innovation, i_post_for_id, i_post_for_name, i_title, i_contact_email,
				i_phone_country_code, i_phone_number, i_short_description, i_long_description,
				i_salary_type, i_city_name, i_location_id, i_job_type, i_salary_details, i_qualification, i_experience);
  else
    set o_id = 0;
  end if;
END$$

CREATE PROCEDURE `save_innovation_details_from_application` (
            in i_user_id int(11),
            in i_email varchar(500),
            in i_innovation_id int(11),
            in i_name varchar(500),
            in i_location_id varchar(50),
            in i_type_id varchar(50),
            in i_clinical_area_id varchar(50),
            in i_stage_id varchar(50),
            in i_org_type_id varchar(50),
            in i_source_id varchar(50),
            in i_description text,
            in i_fund_raised varchar(50),
            in i_created_on varchar(50),
            out o_id int(11))
begin
DECLARE u_id INT default 0;
DECLARE v_version_code INT default 0;
DECLARE i_facebook_url VARCHAR(500);
DECLARE i_twitter_url VARCHAR(500);
DECLARE i_google_plus_url VARCHAR(500);
DECLARE i_url_1 VARCHAR(500);
DECLARE i_url_2 VARCHAR(500);
DECLARE i_url_3 VARCHAR(500);
  select user_id into u_id from user_master where user_id = i_user_id and email = i_email;
  if u_id > 0 then
	 if i_innovation_id = 0 then
	   /** insert */
  	 insert into innovation_master(created_on) values(now());
  	 set o_id = (select LAST_INSERT_ID());
  	 insert into user_innovations(user_id, innovation_id, is_admin, joined_on) values(u_id, o_id, 'Y', i_created_on);
  	 insert into innovation_details(innovation_id,
  		name, location_id, innovation_type_id, clinical_area_id, innovation_stage_id, org_type_id, innovation_source_id, description, fund_raised)
        values(o_id, i_name, i_location_id, i_type_id, i_clinical_area_id, i_stage_id, i_org_type_id, i_source_id, i_description, i_fund_raised);
    else
      select innovation_id into o_id from user_master m,user_innovations i
       where m.user_id = i.user_id and m.email = i_email and i.user_id = u_id and innovation_id = i_innovation_id;
      select max(version_code) into v_version_code from innovation_details where innovation_id = i_innovation_id;
      set v_version_code = v_version_code + 1;
      update innovation_details set is_current = 'N' where innovation_id = o_id;
	    select facebook_url as i_facebook_url, twitter_url as i_twitter_url,
  		  google_plus_url as i_google_plus_url, url_1 as i_url_1, url_2 as i_url_2, url_3 as i_url_3
		    from innovation_details where innovation_id = o_id and version_code = v_version_code - 1;
      insert into innovation_details(innovation_id, version_code,
  		name, location_id, innovation_type_id, clinical_area_id, innovation_stage_id, org_type_id, innovation_source_id, description,
  			facebook_url, twitter_url, google_plus_url, url_1, url_2, url_3, fund_raised)
        values(o_id, v_version_code ,i_name, i_location_id, i_type_id, i_clinical_area_id, i_stage_id, i_org_type_id, i_source_id, i_description,
  				i_facebook_url, i_twitter_url, i_google_plus_url, i_url_1, i_url_2, i_url_3, i_fund_raised);
	 end if;
  else
    set o_id = 0;
  end if;
END$$

CREATE FUNCTION `f_add_new_cohort_3` (
            v_team_id varchar(100),
            v_application_id varchar(100)) RETURNS int(11)
DETERMINISTIC
begin
DECLARE v_finished INT default 0;
DECLARE v_user_id INT default 0;
DECLARE v_is_manager varchar(100) default '';
DECLARE v_is_primary_investigator varchar(100) default '';
		DEClARE team_cursor CURSOR FOR
 		select user_id from user_applications where application_id = v_application_id;
 		DECLARE CONTINUE HANDLER
        	FOR NOT FOUND SET v_finished = 1;
		OPEN team_cursor;
 		get_team_member: LOOP
 		FETCH team_cursor INTO v_user_id;
 		IF v_finished = 1 THEN
 			LEAVE get_team_member;
 		END IF;
 		insert into cap_team_members(team_id, user_id, is_manager, is_primary_investigator) values(v_team_id, v_user_id, v_is_manager, v_is_primary_investigator);
 		END LOOP get_team_member;
 		CLOSE team_cursor;
    return 1;
END$$

CREATE PROCEDURE `cap_add_team_to_cohort` (
            in i_cohort_id varchar(50),
            in i_innovation_id varchar(50),
            in i_manager_id int,
            in i_pi_id int,
            out o_id int(11))
begin
  declare v_is_team int default 0;
  declare v_team_id int default 0;
  declare v_finished int default 0;
  declare v_user_id int default 0;
  declare v_allow_insert int default 0;
  declare v_innovation_name varchar(100) default '';
  		declare team_cursor cursor for
  			select ui.user_id from user_innovations ui where ui.is_active = 'Y' and ui.innovation_id = i_innovation_id order by ui.is_admin;
  		declare continue handler
  			for not found set v_finished = 1;
  select count(*) into v_allow_insert from cap_team_master where cohort_id = i_cohort_id and innovation_id = i_innovation_id;
  if v_allow_insert > 0 then
  set o_id = -1;
  else
  	select name into v_innovation_name from innovation_master im left join innovation_details id using(innovation_id) where im.innovation_id = i_innovation_id and id.is_current = 'Y';
  	insert into cap_team_master(team_name, cohort_id, innovation_id) values(v_innovation_name, i_cohort_id, i_innovation_id);
	set v_team_id = (select LAST_INSERT_ID());
	if v_is_team > 0 then
    	set o_id = -1; -- team wasn't created successfully
  	else
  		open team_cursor;
  		get_team_member: loop
  		fetch team_cursor into v_user_id;
  		if v_finished = 1 then
  			leave get_team_member;
  		end if;
		insert into cap_team_members(user_id, team_id) values(v_user_id, v_team_id);
		end loop get_team_member;
		close team_cursor;
		update cap_team_members set is_manager = true where user_id = i_manager_id and team_id = v_team_id;
		update cap_team_members set is_primary_investigator = true where user_id = i_pi_id and team_id = v_team_id;
    set o_id = v_team_id;
  end if;
  end if;
END$$

CREATE PROCEDURE `cap_add_member_to_team` (
            in i_user_id varchar(50),
            in i_team_id varchar(50),
            out o_id int(11))
begin
  declare v_is_team_member int default 0;
 	select count(*) into v_is_team_member from cap_team_members where user_id = i_user_id and team_id = i_team_id;
  if v_is_team_member > 0 then
    	set o_id = -1; -- member already in team, can't readd
 	else
		  insert into cap_team_members(user_id, team_id) values(i_user_id, i_team_id);
      set o_id = LAST_INSERT_ID();
  end if;
END$$

CREATE PROCEDURE `cap_add_team_to_unlinked_cohort` (
            in i_cohort_id varchar(50),
            in i_team_name varchar(50),
            in i_manager_id int,
            in i_pi_id int,
            out o_id int(11))
begin
  declare v_is_team int default 0;
  declare v_team_id int default 0;
  select team_id into v_is_team from cap_team_master where team_name = i_team_name;
  if v_is_team > 0 then
    set o_id = -1;
  else
    insert into cap_team_master(cohort_id, team_name) values(i_cohort_id, i_team_name);
    set v_team_id = LAST_INSERT_ID();
    insert into cap_team_members(user_id, team_id, is_manager) values(i_manager_id, v_team_id, 1);
    insert into cap_team_members(user_id, team_id, is_primary_investigator) values(i_pi_id, v_team_id, 1);
    set o_id = 1;
  end if;
END$$

CREATE PROCEDURE `cap_mark_milestone_as_complete` (
            in i_mil_team_id int(11),
            in i_user_id int(11),
            out o_id int(11))
begin
  declare v_is_team int default 0;
  declare v_team_id int default 0;
  declare v_team_name varchar(100) default '';
  declare v_milestone_name varchar(100) default '';
  declare v_user_id int default 0;
  declare v_coach_id int default 0;
  declare v_finished int default 0;
  declare v_notification_text varchar(500) default '';
      declare team_users_cursor cursor for
        select ctm.user_id from cap_team_members ctm left join cap_team_milestones ctms using(team_id) where ctms.mil_team_id = i_mil_team_id and !ctm.is_manager;
      declare continue handler
        for not found set v_finished = 1;
    select team_id, team_name, milestone_description into v_team_id, v_team_name, v_milestone_name from cap_team_milestones ctms left join cap_team_master ctm using(team_id) left join cap_milestones_master using(milestone_id) where ctms.mil_team_id = i_mil_team_id;
    set v_notification_text = concat('Team ', v_team_name, ' completed ',v_milestone_name, ' milestone');
    select user_id into v_coach_id from user_master where is_coach limit 1;
    update cap_team_milestones set is_completed = true, submission_date = now() where mil_team_id = i_mil_team_id;
    open team_users_cursor;
    get_team_users_member: loop
    fetch team_users_cursor into v_user_id;
      if v_finished = 1 then
        leave get_team_users_member;
      end if;
      insert into cap_system_notifications(notif_description, user_id, team_id) values(v_notification_text, v_user_id, v_team_id);
    end loop get_team_users_member;
    close team_users_cursor;
    insert into cap_system_notifications(notif_description, user_id, team_id) values(v_notification_text, v_coach_id, v_team_id);
    update cap_team_master set status_before = status_current, status_current = 'submitted_for_review' where team_id = v_team_id;
    set o_id = 1;
END$$

CREATE PROCEDURE `cap_approve_plan_for_review` (
            in i_team_id varchar(50),
            out o_id int(11))
begin
      declare v_can_approve int default 0;
      declare v_user_id int default 0;
      declare v_finished int default 0;
      declare v_team_name varchar(500) default '';
      declare v_notification_text varchar(500) default '';
      declare team_cursor cursor for
        select user_id from cap_team_members where team_id = i_team_id;
      declare continue handler
      for not found set v_finished = 1;
      select count(*), concat_ws(' - ', team_name, cohort_name) into v_can_approve, v_team_name from cap_team_master ctm left join cap_cohort_master ccm using(cohort_id) where status_current = 'plan_submitted' and team_id = i_team_id;
      if v_can_approve > 0 then
          update cap_team_master set status_before = status_current where team_id = i_team_id;
          update cap_team_master set status_current = 'active' where team_id = i_team_id;
          set v_notification_text = concat_ws(' ', v_team_name, ' - project plan has been approved by the coach');
           open team_cursor;
              get_team_member: loop
              fetch team_cursor into v_user_id;
              if v_finished = 1 then
             leave get_team_member;
             end if;
             insert into cap_system_notifications(user_id, team_id, notif_description) values(v_user_id, i_team_id, v_notification_text);
             end loop get_team_member;
          close team_cursor;
          set o_id = 1;   -- success
      else
          set o_id = -1;   -- this plan is already submitted or not in 'new' state
      end if;
END$$

CREATE PROCEDURE `add_new_cohort_2` (
			in i_opportunity_id varchar(100),
			in v_cohort_id varchar(100))
begin
DECLARE v_finished INT default 0;
DECLARE v_application_id INT default 0;
DECLARE v_innovation_id INT default 0;
DECLARE v_team_id INT default 0;
DECLARE v_output INT default 0;
DECLARE v_application_name VARCHAR(100) default '';
		DEClARE app_id_cursor CURSOR FOR
		select owm.application_id from opportunity_winner_master owm left join application_master am on am.application_id = owm.application_id where opportunity_id = i_opportunity_id;
		DECLARE CONTINUE HANDLER
        	FOR NOT FOUND SET v_finished = 1;
    OPEN app_id_cursor;
    get_app_id: LOOP
 		FETCH app_id_cursor INTO v_application_id;
 		IF v_finished = 1 THEN
 			LEAVE get_app_id;
 		END IF;
 		select am.innovation_id, ad.application_name into v_innovation_id, v_application_name from application_master am left join application_details ad using(application_id) where am.application_id = v_application_id;
 		insert into cap_team_master(team_name, cohort_id, innovation_id) values(v_application_name, v_cohort_id, v_innovation_id);
		set v_team_id = (select LAST_INSERT_ID());
			call add_new_cohort_3(v_team_id, v_application_id);
 		END LOOP get_app_id;
 		CLOSE app_id_cursor;
END$$

CREATE PROCEDURE `add_new_cohort_1` (
            in i_cohort_name varchar(100),
            in i_opportunity_id varchar(500),
            in i_opportunity_name varchar(100),
            in i_start_date varchar(100),
            in i_end_date varchar(100),
            in i_status varchar(100),
            out o_id int(11))
begin
DECLARE v_cohort_id INT default 0;
	if i_status = 'existing' then
		insert into cap_cohort_master(cohort_name, opportunity_id, opportunity_name, start_date, end_date, is_linked_to_opp)
			values(i_cohort_name, i_opportunity_id, i_opportunity_name, i_start_date, i_end_date, true);
		set v_cohort_id = (select LAST_INSERT_ID());
		call add_new_cohort_2(i_opportunity_id, v_cohort_id);
		set o_id = v_cohort_id;
	else
		insert into cap_cohort_master(cohort_name, opportunity_name, start_date, end_date, is_linked_to_opp)
			values(i_cohort_name, i_opportunity_name, i_start_date, i_end_date, false);
		set o_id = (select LAST_INSERT_ID());
	end if;
END$$

CREATE PROCEDURE `add_new_cohort_3` (
            in v_team_id varchar(100),
            in v_application_id varchar(100))
begin
DECLARE v_finished INT default 0;
DECLARE v_user_id INT default 0;
DECLARE v_is_admin varchar(100) default '';
DECLARE v_is_primary_investigator varchar(100) default '';
DECLARE v_bool_is_admin boolean default false;
DECLARE v_bool_is_primary_investigator boolean default false;
		DEClARE team_cursor CURSOR FOR
 		select user_id, is_admin, is_principal from user_applications where application_id = v_application_id;
 		DECLARE CONTINUE HANDLER
        	FOR NOT FOUND SET v_finished = 1;
		OPEN team_cursor;
 		get_team_member: LOOP
 		FETCH team_cursor INTO v_user_id, v_is_admin, v_is_primary_investigator;
 		IF v_finished = 1 THEN
 			LEAVE get_team_member;
 		END IF;
 		if v_is_admin = 'Y' and !v_bool_is_admin then
 			set v_bool_is_admin = true;
 		else
 			set v_bool_is_admin = false;
 		end if;
 		if v_is_primary_investigator = 'Y' and !v_bool_is_primary_investigator then
 			set v_bool_is_primary_investigator = true;
 		else
 			set v_bool_is_primary_investigator = false;
 		end if;
 		insert into cap_team_members(team_id, user_id, is_manager, is_primary_investigator) values(v_team_id, v_user_id, v_bool_is_admin, v_bool_is_primary_investigator);
 		END LOOP get_team_member;
 		CLOSE team_cursor;
END$$

CREATE PROCEDURE `cap_accept_or_reject_milestone` (
            in i_mil_team_id int(11),
            in i_user_id int(11),
            in i_is_accepted boolean,
            out o_id int(11))
begin
  declare v_is_team int default 0;
  declare v_team_id int default 0;
  declare v_team_name varchar(100) default '';
  declare v_milestone_name varchar(100) default '';
  declare v_user_id int default 0;
  declare v_coach_id int default 0;
  declare v_finished int default 0;
  declare v_notification_text varchar(500) default '';
  declare v_is_pending_approval int default 0;
  declare v_is_team_completed int default 0;
      declare team_users_cursor cursor for
        select ctm.user_id from cap_team_members ctm left join cap_team_milestones ctms using(team_id) where ctms.mil_team_id = i_mil_team_id;
      declare continue handler
        for not found set v_finished = 1;
    select team_id, team_name, milestone_description into v_team_id, v_team_name, v_milestone_name from cap_team_milestones ctms
        left join cap_team_master ctm using(team_id) left join cap_milestones_master using(milestone_id) where ctms.mil_team_id = i_mil_team_id;
    select user_id into v_coach_id from user_master where is_coach limit 1;
    if i_is_accepted then
      update cap_team_milestones set is_approved = true, approval_date = now() where mil_team_id = i_mil_team_id;
      set v_notification_text = concat('Coach has approved ',v_milestone_name, ' milestone');
    else
      update cap_team_milestones set is_completed = false where mil_team_id = i_mil_team_id;
      set v_notification_text = concat('Coach has rejected ',v_milestone_name, ' milestone');
    end if;
    open team_users_cursor;
    get_team_users_member: loop
    fetch team_users_cursor into v_user_id;
      if v_finished = 1 then
        leave get_team_users_member;
      end if;
      insert into cap_system_notifications(notif_description, user_id, team_id) values(v_notification_text, v_user_id, v_team_id);
    end loop get_team_users_member;
    close team_users_cursor;
      select count(*) into v_is_pending_approval from cap_team_milestones where team_id = v_team_id and is_completed = true;
      select count(*) into v_is_team_completed from cap_team_milestones where team_id = v_team_id and is_approved = true;
      if v_is_pending_approval = v_is_team_completed then
        update cap_team_master set status_before = status_current, status_current = 'active' where team_id = v_team_id;
      end if;
    set o_id = 1;
END$$

CREATE PROCEDURE `add_private_members_by_opportunity` (
            in i_room_id varchar(50),
            in i_user_id varchar(50),
            out o_id int(11))
begin
  declare v_is_member int default 0;
  select member_id into v_is_member from forum_private_room_members where room_id = i_room_id and user_id = i_user_id;
  if v_is_member > 0 then
    set o_id = -1;
  else
    insert into forum_private_room_members(room_id, user_id) values(i_room_id,i_user_id);
    set o_id = 1;
  end if;
END$$

CREATE PROCEDURE `cap_remove_member_from_team` (
            in i_user_id varchar(50),
            in i_team_id varchar(50),
            out o_id int(11))
begin
  declare v_pi_id int default 0;
  declare v_manager_id int default 0;
 	select user_id into v_pi_id from cap_team_members where team_id = i_team_id and is_primary_investigator = 1;
 	select user_id into v_manager_id from cap_team_members where team_id = i_team_id and is_manager = 1;
  if i_user_id = v_pi_id then
      	set o_id = -1;
  elseif i_user_id = v_manager_id then
 	      set o_id = -2;
 	else
		  delete from cap_team_members where team_id = i_team_id  and user_id = i_user_id;
      set o_id = 1;
  end if;
END$$

CREATE PROCEDURE `forum_create_new_room` (
            in i_room_name varchar(500),
            in i_description varchar(5000),
            in i_access_type varchar(50),
            in i_moderator_id varchar(50),
            out o_id int(11))
BEGIN
  DECLARE main_id INT default 0;
  declare v_room_val int;
  select count(*) into v_room_val from forum_rooms_master where room_name =  i_room_name and is_active;
  IF v_room_val > 0 then
    set o_id = -1;
  ELSE
    insert into forum_rooms_master(room_name, description, access_type) values(i_room_name, i_description, i_access_type);
    set o_id = (select LAST_INSERT_ID());
    insert into forum_rel_room_moderators(room_id, moderator_id) values(o_id, i_moderator_id);
  END IF;
END$$

CREATE PROCEDURE `forum_create_new_post` (
            in i_description varchar(5000),
            in i_thread_id varchar(50),
            in i_user_id varchar(50),
            out o_id int(11))
begin
  declare main_id INT default 0;
  declare v_access_type varchar(100) default null;
  declare v_temp_count_flag int default 0;
  declare v_is_allowed boolean default false;
  declare v_error_code int default 0;
  declare v_room_id int default 0;
  select count(*) into v_temp_count_flag from forum_threads_master where  is_closed = false and thread_id = i_thread_id;
  if v_temp_count_flag > 0 then
    select access_type, room_id into v_access_type, v_room_id from forum_threads_master ftm left join forum_rooms_master frm using(room_id) where ftm.thread_id = i_thread_id;
    if v_access_type = 'private' then
      select count(*) into v_temp_count_flag from ((select member_id from forum_private_room_members where user_id = i_user_id and room_id = v_room_id) union (select moderator_id from forum_rel_room_moderators rrm left join forum_moderators_master mm using(moderator_id) where rrm.room_id = v_room_id and mm.user_id = i_user_id and mm.is_active)) as dump;
      if v_temp_count_flag > 0 then
        set v_is_allowed = true;
      else
        set v_error_code = -1;
      end if;
    else
      select count(*) into v_temp_count_flag from forum_blocked_members where user_id = i_user_id;
      if v_temp_count_flag > 0 then
        set v_error_code = -2;
      else
          set v_is_allowed = true;
      end if;
    end if;
  else
    set v_error_code = -3;
  end if;
  if v_is_allowed then
  insert into forum_posts_master(thread_id, user_id, description) values (i_thread_id, i_user_id, i_description);
    set o_id = (select LAST_INSERT_ID());
  else
    set o_id = v_error_code;
  end if;
END$$

CREATE PROCEDURE `forum_create_new_thread` (
            in i_title varchar(500),
            in i_description varchar(5000),
            in i_room_id varchar(50),
            in i_user_id varchar(50),
            out o_id int(11))
begin
  declare main_id INT default 0;
  declare v_access_type varchar(100) default null;
  declare v_temp_count_flag int default 0;
  declare v_is_allowed boolean default false;
  declare v_error_code int default 0;
  select access_type into v_access_type from forum_rooms_master where room_id = i_room_id;
  if v_access_type = 'private' then
    select count(*) into v_temp_count_flag from ((select member_id from forum_private_room_members where user_id = i_user_id and room_id = i_room_id) union (select moderator_id from forum_rel_room_moderators rrm left join forum_moderators_master mm using(moderator_id) where rrm.room_id = i_room_id and mm.user_id = i_user_id and mm.is_active)) as dump;
    if v_temp_count_flag > 0 then
      set v_is_allowed = true;
    else
      set v_error_code = -1;
    end if;
  else
      select count(*) into v_temp_count_flag from forum_blocked_members where user_id = i_user_id;
      if v_temp_count_flag > 0 then
        set v_error_code = -2;
      else
         set v_is_allowed = true;
      end if;
  end if;
  if v_is_allowed then
    insert into forum_threads_master(room_id, title, description, user_id) values(i_room_id, i_title, i_description, i_user_id);
    set o_id = (select LAST_INSERT_ID());
  else
    set o_id = v_error_code;
  end if;
END$$

CREATE PROCEDURE `forum_edit_room` (
            in i_room_id varchar(50),
            in i_room_name varchar(500),
            in i_description varchar(5000),
            in i_access_type varchar(50),
            in i_moderator_id varchar(50),
            out o_id int(11))
begin
declare v_access_type varchar(100) default null;
declare v_allowed_edit int default 0;
select count(*) into v_allowed_edit from forum_rooms_master where room_name = i_room_name and is_active and room_id != i_room_id;
if v_allowed_edit > 0 then
set o_id = -1;
else
 select access_type into v_access_type from forum_rooms_master where room_id = i_room_id;
  update forum_rooms_master set room_name = i_room_name, access_type = i_access_type, description = i_description where room_id = i_room_id;
  if i_access_type <> v_access_type then
    if v_access_type = 'private' then -- if existing access_type is private i.e. it has changed to public
      delete from forum_private_room_members where room_id = i_room_id;
    elseif v_access_type = 'public' then -- if existing access_type is public i.e. it has changed to private
      select distinct pm.user_id from forum_posts_master pm left join forum_threads_master tm using(thread_id) left join forum_rooms_master rm using(room_id) where pm.is_active and tm.is_closed = false and rm.room_id = i_room_id and pm.user_id not in(select user_id from forum_blocked_members);
      call public_to_private(i_room_id);
    end if;
  end if;
    delete from forum_rel_room_moderators where room_id = i_room_id;
    insert into forum_rel_room_moderators(room_id, moderator_id) values(i_room_id, i_moderator_id);
  set o_id = 1;
 end if;
END$$

CREATE PROCEDURE `forum_delete_post` (
            in i_post_id varchar(50),
            in i_user_id varchar(50),
            out o_id int(11))
begin
  declare main_id INT default 0;
  declare v_access_type varchar(100) default null;
  declare v_temp_count_flag int default 0;
  declare v_is_allowed varchar(100) default null;
  declare v_error_code int default 0;
  declare v_is_admin int default 0;
  declare v_is_moderator int default 0;
  declare v_is_member int default 0;
  declare v_posted_by_user_id int default 0;
  declare v_posted_by_is_admin int default 0;
  declare v_posted_by_is_moderator int default 0;
  declare v_is_blocked int default 0;
  declare v_room_id int default 0;
  select count(*) into v_is_blocked from forum_blocked_members where user_id = i_user_id;
  if v_is_blocked > 0 then
  set v_error_code=-5;
  else
  select count(*) into v_temp_count_flag from forum_threads_master tm left join forum_posts_master pm using(thread_id) where tm.is_closed = false and pm.post_id = i_post_id;
  if v_temp_count_flag > 0 then
    select user_id into v_posted_by_user_id from forum_posts_master where post_id = i_post_id;
    select room_id into v_room_id from forum_posts_master pm left join forum_threads_master tm using(thread_id) left join forum_rooms_master rm using(room_id) where pm.post_id = i_post_id;
    select count(*) into v_is_admin from user_master where user_id = i_user_id and is_admin;
    if v_is_admin > 0 then
      select count(*) into v_posted_by_is_moderator from forum_moderators_master mm left join forum_rel_room_moderators rrm using(moderator_id) where mm.user_id = i_user_id and rrm.room_id = v_room_id;
      if v_posted_by_is_moderator > 0 then
        set v_error_code=-1;
      else
        set v_is_allowed= 'admin';
      end if;
    else
      select count(*) into v_is_moderator from forum_moderators_master fmm left join forum_rel_room_moderators rrm using(moderator_id) where fmm.user_id = i_user_id and rrm.room_id = v_room_id;
      if v_is_moderator > 0 then -- he is a moderator, check for member
        select count(*) into v_posted_by_is_admin from user_master where is_admin and user_id = v_posted_by_user_id;
        if v_posted_by_is_admin > 0 then
          set v_error_code = -2;
        else
           set v_is_allowed = 'moderator';
        end if;
      else
          if v_posted_by_user_id = i_user_id then
           set v_is_allowed = 'user';
          else
            set  v_error_code = -3;
          end if;
      end if;
    end if;
  else
   set v_error_code = -4;
  end if;
  end if;
  if v_is_allowed is not null then
    update forum_posts_master set is_active = false, deleted_by = v_is_allowed where post_id = i_post_id;
    set o_id = i_post_id;
  else
    set o_id = v_error_code;
  end if;
END$$

CREATE PROCEDURE `cap_update_team_composition` (
            in i_user_id varchar(50),
            in i_team_id varchar(50),
            in i_status varchar(50),
            out o_id int(11))
begin
      set o_id = -1;
      if i_status = 'manager' then
      update cap_team_members set is_manager = 0 where is_manager = 1 and team_id = i_team_id;
      update cap_team_members set is_manager = 1 where user_id = i_user_id and team_id = i_team_id;
      set o_id = 1;
      else
      update cap_team_members set is_primary_investigator = 0 where is_primary_investigator = 1 and team_id = i_team_id;
      update cap_team_members set is_primary_investigator = 1 where user_id = i_user_id and team_id = i_team_id;
      set o_id = 1;
      end if;
END$$

CREATE PROCEDURE `cap_submit_for_review` (
            in i_team_id varchar(50),
            out o_id int(11))
begin
      declare v_can_submit int(11) default 0;
      declare v_user_id int default 0;
      declare v_finished int default 0;
      declare v_team_name varchar(500) default '';
      declare v_notification_text varchar(500) default '';
      declare v_coach_id int default 0;
      declare team_cursor cursor for
        select user_id from cap_team_members where team_id = i_team_id and !is_manager;
      declare continue handler
      for not found set v_finished = 1;
      select count(*), concat_ws(' - ', team_name, cohort_name) into v_can_submit, v_team_name from cap_team_master ctm left join cap_cohort_master ccm using(cohort_id) where status_current = 'new' and team_id = i_team_id;
      if v_can_submit > 0 then
          update cap_team_master set status_before = status_current where team_id = i_team_id;
          update cap_team_master set status_current = 'plan_submitted' where team_id = i_team_id;
          set v_notification_text = concat_ws(' ', v_team_name, ' plan submitted');
          open team_cursor;
              get_team_member: loop
              fetch team_cursor into v_user_id;
              if v_finished = 1 then
             leave get_team_member;
             end if;
             insert into cap_system_notifications(user_id, team_id, notif_description) values(v_user_id, i_team_id, v_notification_text);
             end loop get_team_member;
          close team_cursor;
          select user_id into v_coach_id from user_master where is_coach;
          insert into cap_system_notifications(user_id, notif_description) values(v_coach_id, v_notification_text);
          set o_id = 1;   -- success
      else
          set o_id = -1;   -- this plan is already submitted or not in 'new' state
      end if;
END$$

CREATE PROCEDURE `cap_update_team_milestone` (
            in i_mil_team_id varchar(50),
            in i_target_date varchar(50),
            out o_id int(11))
begin
      declare v_team_id int(11) default 0;
      declare v_cohort_id int(11) default 0;
      declare cohort_start_date int(11) default 0;
      declare cohort_end_date int(11) default 0;
      select count(*) into v_cohort_id from cap_cohort_master where cohort_id in(select cohort_id from cap_team_master where team_id in(select team_id from cap_team_milestones where mil_team_id = i_mil_team_id));
      if v_cohort_id > 0 then
          update cap_team_milestones set target_date = i_target_date where mil_team_id = i_mil_team_id;
          delete from cap_milestone_activity where mil_team_id = i_mil_team_id;
          set o_id = 1;
      else
          set o_id = -1;
      end if;
END$$

CREATE FUNCTION `fn_return_id` () RETURNS int(11)
DETERMINISTIC
return @id$$

CREATE PROCEDURE `close_thread` (
            in i_thread_id int,
            in i_user_id int,
            out o_id int(11))
BEGIN
declare v_is_moderator int default 0;
  select count(*) into v_is_moderator from forum_moderators_master mm left join forum_rel_room_moderators rrm using(moderator_id) where mm.user_id = i_user_id and rrm.room_id = (select room_id from forum_threads_master where thread_id = i_thread_id);
  if v_is_moderator > 0 then
    update forum_threads_master set is_closed = true where thread_id = i_thread_id;
    set o_id = i_thread_id;
  else
    set o_id = -1;
  end if;
END$$

DELIMITER ;

CREATE
  SQL SECURITY DEFINER
  VIEW `v_cap_team_count_incomp_mile`
AS
select `ctmu1`.`team_id` AS `team_id`,count(`ctmsu1`.`mil_team_id`) AS `num_of_uncompleted_milestones` from (`cap_team_master` `ctmu1` left join `cap_team_milestones` `ctmsu1` on(((`ctmu1`.`team_id` = `ctmsu1`.`team_id`) and (not(`ctmsu1`.`is_approved`))))) group by `ctmu1`.`team_id`;

CREATE
  SQL SECURITY DEFINER
  VIEW `v_cap_team_count_total_mile`
AS
select `cap_team_milestones`.`team_id` AS `team_id`,count(0) AS `total_milestones` from `cap_team_milestones` group by `cap_team_milestones`.`team_id`;

CREATE
  SQL SECURITY DEFINER
  VIEW `v_cap_team_milestone_prev_max_tdate`
AS
select `cap_team_milestones`.`team_id` AS `team_id`,max(`cap_team_milestones`.`target_date`) AS `mtdate` from `cap_team_milestones` where ((not(`cap_team_milestones`.`is_approved`)) and (`cap_team_milestones`.`target_date` < curdate())) group by `cap_team_milestones`.`team_id`;

CREATE
  SQL SECURITY DEFINER
  VIEW `v_cap_team_delayed_milestone`
AS
select `ctm1`.`team_id` AS `team_id`,`ctm1`.`milestone_id` AS `milestone_id`,`cmm`.`milestone_description` AS `milestone_description`,`ctm1`.`target_date` AS `target_date`,greatest(0,(to_days(curdate()) - to_days(`ctm1`.`target_date`))) AS `num_of_days_delayed` from ((`cap_team_milestones` `ctm1` join `v_cap_team_milestone_prev_max_tdate` `ctm2` on(((`ctm1`.`target_date` = `ctm2`.`mtdate`) and (`ctm1`.`team_id` = `ctm2`.`team_id`)))) left join `cap_milestones_master` `cmm` on((`cmm`.`milestone_id` = `ctm1`.`milestone_id`))) group by `ctm1`.`team_id`;

CREATE
  SQL SECURITY DEFINER
  VIEW `v_cap_team_milestone_last_sdate`
AS
select `cap_team_milestones`.`team_id` AS `team_id`,`cap_team_milestones`.`mil_team_id` AS `mil_team_id`,max(`cap_team_milestones`.`submission_date`) AS `sbdate` from `cap_team_milestones` where `cap_team_milestones`.`is_approved` group by `cap_team_milestones`.`team_id`;

CREATE
  SQL SECURITY DEFINER
  VIEW `v_cap_team_last_completed_mile`
AS
select `ctm1`.`team_id` AS `team_id`,`ctm1`.`mil_team_id` AS `mil_team_id`,`ctm1`.`submission_date` AS `submission_date`,`cmm`.`milestone_description` AS `milestone_description`,greatest(0,(to_days(`ctm1`.`submission_date`) - to_days(`ctm1`.`target_date`))) AS `num_of_days_delayed` from ((`cap_team_milestones` `ctm1` join `v_cap_team_milestone_last_sdate` `ctm2` on(((`ctm1`.`team_id` = `ctm2`.`team_id`) and (`ctm1`.`submission_date` = `ctm2`.`sbdate`)))) left join `cap_milestones_master` `cmm` on((`cmm`.`milestone_id` = `ctm1`.`milestone_id`))) group by `ctm1`.`team_id`;

CREATE
  SQL SECURITY DEFINER
  VIEW `v_cap_team_milestone_next_min_tdate`
AS
select `cap_team_milestones`.`team_id` AS `team_id`,min(`cap_team_milestones`.`target_date`) AS `mtdate` from `cap_team_milestones` where ((not(`cap_team_milestones`.`is_approved`)) and (`cap_team_milestones`.`target_date` >= curdate())) group by `cap_team_milestones`.`team_id`;

CREATE
  SQL SECURITY DEFINER
  VIEW `v_cap_team_upcoming_milestone`
AS
select `ctm1`.`team_id` AS `team_id`,`ctm1`.`milestone_id` AS `milestone_id`,`cmm`.`milestone_description` AS `milestone_description`,`ctm1`.`target_date` AS `target_date` from ((`cap_team_milestones` `ctm1` join `v_cap_team_milestone_next_min_tdate` `ctm2` on(((`ctm1`.`target_date` = `ctm2`.`mtdate`) and (`ctm1`.`team_id` = `ctm2`.`team_id`)))) left join `cap_milestones_master` `cmm` on((`cmm`.`milestone_id` = `ctm1`.`milestone_id`))) group by `ctm1`.`team_id`;

CREATE
  SQL SECURITY DEFINER
  VIEW `view_cap_cohort_dashboard`
AS
select `ctm`.`cohort_id` AS `cohort_id`,`ccm`.`cohort_name` AS `cohort_name`,`ctm`.`team_id` AS `team_id`,`ctm`.`team_name` AS `team_name`,`ctm`.`status_current` AS `status_current`,concat_ws(' ',`lcm`.`mil_team_id`) AS `lcm_mil_team_id`,concat_ws(' ',`lcm`.`milestone_description`) AS `lcm_milestone_description`,concat_ws(' ',`lcm`.`num_of_days_delayed`) AS `lcm_num_of_days_delayed`,concat_ws(' ',`cdm`.`milestone_id`) AS `cdm_milestone_id`,concat_ws(' ',`cdm`.`milestone_description`) AS `cdm_milestone_description`,date_format(concat_ws(' ',`cdm`.`target_date`),'%M %d, %Y') AS `cdm_target_date`,concat_ws(' ',`cdm`.`num_of_days_delayed`) AS `cdm_num_of_days_delayed`,concat_ws(' ',`cum`.`milestone_id`) AS `cum_milestone_id`,concat_ws('',`cum`.`milestone_description`) AS `cum_milestone_description`,date_format(concat_ws('',`cum`.`target_date`),'%M %d, %Y') AS `cum_target_date`,concat_ws(' ',`ud1`.`first_name`,`ud1`.`last_name`) AS `team_manager`,concat_ws('',`ud1`.`user_id`) AS `team_manager_user_id`,concat_ws(' ',`ud2`.`first_name`,`ud2`.`last_name`) AS `primary_investigator`,concat_ws('',`ud2`.`user_id`) AS `primary_investigator_user_id`,group_concat(' ',concat_ws(' ',`ud3`.`first_name`,`ud3`.`last_name`) separator ',') AS `team_members`,`ctms1`.`total_milestones` AS `total_milestones`,(`ctms1`.`total_milestones` - `ctms2`.`num_of_uncompleted_milestones`) AS `num_of_completed_milestones` from ((((((((((((`cap_team_master` `ctm` left join `cap_cohort_master` `ccm` on((`ctm`.`cohort_id` = `ccm`.`cohort_id`))) left join `cap_team_members` `ctmm1` on(((`ctm`.`team_id` = `ctmm1`.`team_id`) and `ctmm1`.`is_manager`))) left join `user_details` `ud1` on((`ctmm1`.`user_id` = `ud1`.`user_id`))) left join `cap_team_members` `ctmm2` on(((`ctm`.`team_id` = `ctmm2`.`team_id`) and `ctmm2`.`is_primary_investigator`))) left join `user_details` `ud2` on((`ctmm2`.`user_id` = `ud2`.`user_id`))) left join `cap_team_members` `ctmm3` on(((`ctm`.`team_id` = `ctmm3`.`team_id`) and (not(`ctmm3`.`is_manager`)) and (not(`ctmm3`.`is_primary_investigator`))))) left join `user_details` `ud3` on((`ctmm3`.`user_id` = `ud3`.`user_id`))) left join `v_cap_team_count_total_mile` `ctms1` on((`ctms1`.`team_id` = `ctm`.`team_id`))) left join `v_cap_team_count_incomp_mile` `ctms2` on((`ctms2`.`team_id` = `ctm`.`team_id`))) left join `v_cap_team_last_completed_mile` `lcm` on((`lcm`.`team_id` = `ctm`.`team_id`))) left join `v_cap_team_delayed_milestone` `cdm` on((`cdm`.`team_id` = `ctm`.`team_id`))) left join `v_cap_team_upcoming_milestone` `cum` on((`cum`.`team_id` = `ctm`.`team_id`))) where (`ctm`.`cohort_id` = `fn_return_id`()) group by `ctm`.`team_id` order by `ctm`.`team_name`;

CREATE
  SQL SECURITY DEFINER
  VIEW `view_cap_user_dashboard`
AS
select concat_ws('',`ctm`.`cohort_id`) AS `cohort_id`,`ccm`.`cohort_name` AS `cohort_name`,`ctm`.`team_name` AS `team_name`,concat_ws('',`ctm`.`team_id`) AS `team_id`,`ctm`.`status_current` AS `status_current`,concat_ws('',`lcm`.`mil_team_id`) AS `lcm_mil_team_id`,concat_ws('',`lcm`.`milestone_description`) AS `lcm_milestone_description`,concat_ws('',`lcm`.`num_of_days_delayed`) AS `lcm_num_of_days_delayed`,concat_ws('',`cdm`.`milestone_id`) AS `cdm_milestone_id`,concat_ws('',`cdm`.`milestone_description`) AS `cdm_milestone_description`,date_format(concat_ws('',`cdm`.`target_date`),'%M %d, %Y') AS `cdm_target_date`,concat_ws('',`cdm`.`num_of_days_delayed`) AS `cdm_num_of_days_delayed`,concat_ws('',`cum`.`milestone_id`) AS `cum_milestone_id`,concat_ws('',`cum`.`milestone_description`) AS `cum_milestone_description`,date_format(concat_ws('',`cum`.`target_date`),'%M %d, %Y') AS `cum_target_date`,concat_ws(' ',`ud1`.`first_name`,`ud1`.`last_name`) AS `team_manager`,concat_ws('',`ud1`.`user_id`) AS `team_manager_user_id`,concat_ws(' ',`ud2`.`first_name`,`ud2`.`last_name`) AS `primary_investigator`,concat_ws('',`ud2`.`user_id`) AS `primary_investigator_user_id`,group_concat(' ',concat_ws(' ',`ud3`.`first_name`,`ud3`.`last_name`) separator ',') AS `team_members`,concat_ws('',`ctms1`.`total_milestones`) AS `total_milestones`,concat_ws('',(`ctms1`.`total_milestones` - `ctms2`.`num_of_uncompleted_milestones`)) AS `num_of_completed_milestones` from (((((((((((((`cap_team_master` `ctm` left join `cap_cohort_master` `ccm` on((`ctm`.`cohort_id` = `ccm`.`cohort_id`))) left join `cap_team_members` `ctmms` on((`ctm`.`team_id` = `ctmms`.`team_id`))) left join `cap_team_members` `ctmm1` on(((`ctm`.`team_id` = `ctmm1`.`team_id`) and `ctmm1`.`is_manager`))) left join `user_details` `ud1` on((`ctmm1`.`user_id` = `ud1`.`user_id`))) left join `cap_team_members` `ctmm2` on(((`ctm`.`team_id` = `ctmm2`.`team_id`) and `ctmm2`.`is_primary_investigator`))) left join `user_details` `ud2` on((`ctmm2`.`user_id` = `ud2`.`user_id`))) left join `cap_team_members` `ctmm3` on(((`ctm`.`team_id` = `ctmm3`.`team_id`) and (not(`ctmm3`.`is_manager`)) and (not(`ctmm3`.`is_primary_investigator`))))) left join `user_details` `ud3` on((`ctmm3`.`user_id` = `ud3`.`user_id`))) left join `v_cap_team_count_total_mile` `ctms1` on((`ctms1`.`team_id` = `ctm`.`team_id`))) left join `v_cap_team_count_incomp_mile` `ctms2` on((`ctms2`.`team_id` = `ctm`.`team_id`))) left join `v_cap_team_last_completed_mile` `lcm` on((`lcm`.`team_id` = `ctm`.`team_id`))) left join `v_cap_team_delayed_milestone` `cdm` on((`cdm`.`team_id` = `ctm`.`team_id`))) left join `v_cap_team_upcoming_milestone` `cum` on((`cum`.`team_id` = `ctm`.`team_id`))) where (`ctmms`.`user_id` = `fn_return_id`()) group by `ctm`.`team_id` order by `ccm`.`cohort_name`;

CREATE
  SQL SECURITY DEFINER
  VIEW `view_expert_login_frequency`
AS
select date_format(`al`.`created_on`,'%M, %Y') AS `display_date`,count(0) AS `experts`,month(`al`.`created_on`) AS `month`,year(`al`.`created_on`) AS `year` from (`application_logs` `al` left join `user_master` `um` on((`um`.`email` = `al`.`user`))) where ((((`al`.`action` = 'log_login') and (`al`.`text` = 'successful')) or (`al`.`text` like '%logged in at%')) and (`um`.`is_expert` = 'Y') and (`al`.`created_on` between (curdate() - interval 3 month) and curdate())) group by month(`al`.`created_on`);

CREATE
  SQL SECURITY DEFINER
  VIEW `view_innovator_login_frequency`
AS
select date_format(`al`.`created_on`,'%M, %Y') AS `display_date`,count(0) AS `innovators`,month(`al`.`created_on`) AS `month`,year(`al`.`created_on`) AS `year` from (`application_logs` `al` left join `user_master` `um` on((`um`.`email` = `al`.`user`))) where ((((`al`.`action` = 'log_login') and (`al`.`text` = 'successful')) or (`al`.`text` like '%logged in at%')) and (`um`.`is_expert` = 'N') and (`al`.`created_on` between (curdate() - interval 3 month) and curdate())) group by month(`al`.`created_on`);

CREATE
  SQL SECURITY DEFINER
  VIEW `view_user_login_frequency`
AS
select date_format(`al`.`created_on`,'%M, %Y') AS `display_date`,count(0) AS `users`,month(`al`.`created_on`) AS `month`,year(`al`.`created_on`) AS `year` from (`application_logs` `al` left join `user_master` `um` on((`um`.`email` = `al`.`user`))) where ((((`al`.`action` = 'log_login') and (`al`.`text` = 'successful')) or (`al`.`text` like '%logged in at%')) and (`al`.`created_on` between (curdate() - interval 3 month) and curdate())) group by month(`al`.`created_on`);
